This repository can be used to experiment with different C++ build types.

# Debugging
The file `debugging.cpp` displays different issues related to pointers.
You can run it with a debugger of your choice to see where your program crashes.
You can also run `valgrind` to get a summary of all memory related issues in your code.
Alternatively, you can enable sanatizers with the `-DENABLE_SANITIZER_ADDRESS=ON` option
and get a similar report, but with less overhead.

The file `debugging-safer.cpp` contains fixes for the mistakes in `debugging.cpp`
and running it with a debugger, valgrind, or sanatizers should not result in any errors.

# Optimizations
The file `optimizing.cpp` contains a simple vector operation that is timed.
You can check the impact that the different build types (debug, release, relwithdebinfo, ...) and 
compiler flags (`-O3, -ffast-math, -march=native, ...`) on the runtime have.

You can also explore the compiled assembler code and play around with the compiler flags here:
https://godbolt.org/z/xhYs5esj1.
