#include <iostream>


void use_after_free(const int*p){
  std::cout << *p + 3 << std::endl;
}

void consume(const int* p){
  std::cout << *p * *p << std::endl;
  delete p;
}

int oob_read(const int* p, const int size){
  int sum = 0;
  for (int i = 0; i < size; ++i) {
    sum += p[i];
  }
  return sum;
}

void oob_write(int* p, const int size, const int val){
  for (int i = 0; i < size; ++i) {
    p[i] = val;
  }
}

int* local_reference(){
  int val = 13;
  int *p = &val;
  return p;
}

struct bad_struct{
  int& val;
};

bad_struct local_reference_struct(){
  int val = 13;
  bad_struct s{val};
  return s;
}


int main(int argc, char** argv) {
  int section = argc == 2 ? std::stoi(argv[1]) : 0;
  // double free
  if (!section || section == 1)
  {
    int *double_free = new int(6);
    consume(double_free);
    delete double_free;
  }

  // use after free
  if (!section || section == 2)
  {
    int *freed = new int(7);
    consume(freed);
    use_after_free(freed);
  }

  // missing free
  if (!section || section == 3)
  {
    int *not_freed = new int(8);
    use_after_free(not_freed);
  }

  const int arr_size = 10;

  // use of uninitialized memory
  if (!section || section == 4)
  {
    int *arr = new int[arr_size];
    oob_write(arr, 9, 0);
    std::cout << oob_read(arr, arr_size) << std::endl;
    delete[] arr;
  }

  // out-of-bounds read
  if (!section || section == 5)
  {
    int *arr = new int[arr_size];
    oob_write(arr, arr_size, 0);
    std::cout << oob_read(arr, 20) << std::endl;
    delete[] arr;
  }

  // out-of-bounds write
  if (!section || section == 6)
  {
    int *arr = new int[arr_size];
    oob_write(arr, 100, 0);
    std::cout << oob_read(arr, arr_size) << std::endl;
    delete[] arr;
  }

  // reference to local variable
  if (!section || section == 7)
  {
    auto p = local_reference();
    std::cout << *p << std::endl;
    delete p;
  }

  // reference to local variable within struct
  if (!section || section == 8)
  {
    auto s = local_reference_struct();
    std::cout << s.val << std::endl;
  }
}
