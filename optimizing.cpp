#include <iostream>
#include <chrono>
#include <vector>
#include <random>

int main(int argc, char** argv){
  const long num_elements = argc > 1 ? std::stol(argv[1]) : 100000000;
  std::vector<double> a(num_elements), b(num_elements);

  for(std::size_t i = 0; i < num_elements; ++i){
    a[i] = static_cast<double>(i * i);
    b[i] = static_cast<double>(i * i * i);
  }

  auto start = std::chrono::steady_clock::now();
  for (std::size_t i = 0; i < num_elements; ++i) {
    b[i] = b[i] + a[i] / 3;
  }
  auto end = std::chrono::steady_clock::now();

  std::cout << "Timing: " << std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count() << "ns" << std::endl;
}
